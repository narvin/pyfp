"""Tools module test suite."""

from collections.abc import Iterable
from typing import Any, Callable

import pytest

from pyfp.tools import compose


class TestCompose:
    """Test suite for the ``compose`` function."""

    @staticmethod
    @pytest.mark.parametrize(
        "fns, inital_value, exp_value",
        (
            (
                (lambda value: f"{value}+first",),
                "hello",
                "hello+first",
            ),
            (
                (
                    lambda value: f"{value}+second",
                    lambda value: f"{value}+first",
                ),
                "hello",
                "hello+first+second",
            ),
        ),
    )
    def test_compose_in_correct_order(
        fns: Iterable[Callable[[Any], Any]], initial_value: Any, exp_value: Any
    ) -> None:
        """Test that the pipeline is composed in the correct order."""
        transform = compose(*fns)
        assert transform(initial_value) == exp_value
