PACKAGE := getversions
SRC_DIR := src
TEST_DIR := test
CODE_DIRS := $(SRC_DIR) $(TEST_DIR)
INSTALL := pip install -e
SORT := python -m isort
FORMAT := python -m black
LINT := python -m pylint -s n
STYLE := python -m pycodestyle
TYPE := python -m mypy --no-error-summary
TEST := python -m pytest -n auto
TEST_ALL := python -m tox --parallel
BUILD := python -m build
TAGS := ctags -R

.PHONY: all install install-check check format format-check lint style type test \
	test-all build deploy tags clean

all: lint style type format-check

install:
	@printf '===== INSTALL =====\n'
	@$(INSTALL) .

install-check:
	@printf '===== INSTALL w/ CHECK DEPS =====\n'
	@$(INSTALL) '.[code_quality,test]'

check: all

format:
	@printf '===== FORMAT =====\n'
	@$(SORT) $(CODE_DIRS)
	@$(FORMAT) $(CODE_DIRS)

lint:
	@printf '===== LINT =====\n'
	@$(LINT) $(CODE_DIRS)

style:
	@printf '===== STYLE =====\n'
	@$(STYLE) $(CODE_DIRS)

type:
	@printf '===== TYPE =====\n'
	@$(TYPE) $(CODE_DIRS)

test:
	@printf '===== TEST =====\n'
	@$(TEST) $(TEST_DIR)

test-all:
	@printf '===== TEST ALL =====\n'
	@$(TEST_ALL)

build:
	@printf '===== BUILD =====\n'
	@$(BUILD)

tags:
	@printf '===== TAGS =====\n'
	@find . -type d -name site-packages | xargs $(TAGS) $(CODE_DIRS)

clean:
	@printf '===== CLEAN =====\n'
	@find $(CODE_DIRS) -type d -name __pycache__ -exec rm -rf {} \+
