"""Tools module for functional programmming."""

from functools import partial, reduce
from itertools import chain
from typing import Any, Callable, Generic, TypeVar


def compose(*fns: Callable[[Any], Any]) -> Callable[[Any], Any]:
    """Compose functions together.

    Creates a pipeline of functions where the output of one function is the input
    of the next one. The resultant function can be called with the initial input to
    run through the entire pipeline.

    The functions are called in reverse order, so the last function will be called
    first on the initial value.

    Args:
        fns: An iterable of functions of one argument to compose together.

    Returns:
        A function of one argument that can be called with the initial value to
        execute the entire pipeline.

    """

    def compose_with_initial_value(initial_value: Any) -> Any:
        return reduce(
            lambda prev_value, next_fn: next_fn(prev_value),
            chain((initial_value,), reversed(fns)),
        )

    return compose_with_initial_value


A = TypeVar("A")
B = TypeVar("B")
C = TypeVar("C")


class Composable(Generic[A, B]):
    """Callable class with a single arg."""
    def __init__(self, func: Callable[[A], B]) -> None:
        self.func = func

    def __call__(self, arg: A) -> B:
        return self.func(arg)

    def __or__(self, other: "Composable[C, A]") -> "Composable[C, B]":
        def execute(arg: C) -> B:
            return self(other(arg))
        return Composable(execute)


def composable(func: Callable[[A], B]) -> Composable[A, B]:
    """Decorator to turn a function into a Composable."""
    return Composable(func)


@composable
def add2(arg: int) -> int:
    """A function that is turned into a composable."""
    return arg + 2


mult3 = Composable(lambda arg: 3 * arg)


def main() -> None:
    """App entry point."""
    comp = add2 | mult3 | add2 | mult3
    print(comp(3), "?== 35")
    comp = mult3 | add2 | mult3 | add2
    print(comp(3), "?== 51")


if __name__ == "__main__":
    main()
